/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef UTILS_HH
#define UTILS_HH

#include <iostream>
#include <cstdio>
#include <cstdlib>

#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>

#include "opencv2/opencv.hpp"

#include "exceptions.hh"

#define DATASET_NAME_STRLEN    256

namespace fs = boost::filesystem;

namespace Utils {
  bool dir_exists(const std::string& dir_name);
  void delete_dir(const std::string& dir_name);
  bool file_exists(const std::string& file_name);
  void create_dir(const std::string& dir_name);
  void compute_mean_stddev(const std::vector< cv::Mat >& images,std::vector< std::pair< float, float > >& ms_vector);
  void compute_mean_stddev(const std::vector< cv::Mat >& images,std::vector< std::pair< float, float > >& ms_vector,
						   const std::vector< cv::Mat >& masks);
  void compute_mean_stddev(const std::vector< cv::Mat >& images,std::vector< std::pair< float, float > >& ms_vector,
						   const unsigned int border);
  void binarize_image(cv::Mat& img);
  bool get_confirmation(const std::string& operation_name);
  void save_debug_mat(const cv::Mat& matrix,const char* out_filename);
  cv::Mat convert_img_visualization(const cv::Mat& original_img);
  cv::Mat convert_img_visualization_color(const cv::Mat& original_img);
  cv::Mat magnify_matrix(const cv::Mat& src_mat,const unsigned int scale_factor);
  void normalize_image_set(std::vector< cv::Mat >& images,const std::vector< std::pair< float, float > >& ms_vector);
  void correlate_valid(const cv::Mat& src,const cv::Mat& kernel,cv::Mat& dst);
  void convolve_valid(const cv::Mat& src,const cv::Mat& kernel,cv::Mat& dst);
  void convolve_full(const cv::Mat& src,const cv::Mat& kernel,cv::Mat& dst);
}

#endif // UTILS_HH
