/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "std_filter_bank_SS.hh"

STD_filter_bank_2D_SS::STD_filter_bank_2D_SS(CmdLine& cmd_line,Parameters& config,cv::RNG& rng) {
  parse_filter_bank_config(config.get_filter_bank_config_filename());

  // Create directories for dumping the results
  m_results_directory = config.get_results_directory();
  m_fb_img_dump_directory = config.get_results_directory()+"/"+m_fb_img_dump_directory;
  m_fb_txt_dump_directory = config.get_results_directory()+"/"+m_fb_txt_dump_directory;
  if (cmd_line.clear()) {
	if (Utils::get_confirmation("clear the results from previous simulation")) {
	  std::cerr << "Cleaning previous simulation's results" << std::endl;
	  if (Utils::dir_exists(m_fb_img_dump_directory))
		Utils::delete_dir(m_fb_img_dump_directory);
	  if (Utils::dir_exists(m_fb_txt_dump_directory))
		Utils::delete_dir(m_fb_txt_dump_directory);
	}
  }
  if (!Utils::dir_exists(m_fb_img_dump_directory))
	Utils::create_dir(m_fb_img_dump_directory);
  if (!Utils::dir_exists(m_fb_txt_dump_directory))
	Utils::create_dir(m_fb_txt_dump_directory);

  // Get system's random number generator
  m_rng = rng;

  // If the resume flag is set, then resume a previous computation, otherwise initialize the filter bank to Gaussian noise
  if (cmd_line.resume()) {
	std::string fb_filename = cmd_line.get_resumed_fb_filename();
	std::cerr << "  Resuming filter bank from file " << fb_filename << std::endl;
	load_filter_bank_from_file(fb_filename);
  }
  else {
	std::cerr << "  Initializing filter bank from Gaussian random noise" << std::endl;
	m_filters.clear();
	unsigned int i_filter = 0;
	if (m_first_filter_uniform) {
	  cv::Mat filter(m_filters_size,m_filters_size,CV_32FC1,cv::Scalar(1.0));
	  m_filters.push_back(filter);
	  i_filter=1;
	}
	for (;i_filter<m_filters_no;++i_filter) {
	  cv::Mat tmp_filter(m_filters_size,m_filters_size,CV_32FC1,cv::Scalar(1.0));
	  m_rng.fill(tmp_filter,cv::RNG::NORMAL,0,1);
	  m_filters.push_back(tmp_filter);
	}
  }
  normalize();
}

void STD_filter_bank_2D_SS::normalize() {
  for (std::vector< cv::Mat >::iterator fb_it=m_filters.begin();fb_it!=m_filters.end();++fb_it) {
	*fb_it /= cv::norm(*fb_it);
  }
}

void STD_filter_bank_2D_SS::parse_filter_bank_config(const std::string& filter_bank_config_filename) {
  po::options_description cfg_file_descr("Filter bank configuration file parameters");
  cfg_file_descr.add_options()
	("filters_size", po::value< unsigned int >(&(m_filters_size)),
	 "Filter's size")
	("filters_no", po::value< unsigned int >(&(m_filters_no)),
	 "Filter bank's cardinality")
	("fb_dump_spacing", po::value< unsigned int >(&(m_fb_dump_spacing)),
	 "Spacing between filters in the image dump")
	("first_filter_uniform", po::value< bool >(&(m_first_filter_uniform))->default_value(true),
	 "Initialize the first filter to a constant value")
	("fb_dump_pixel_size", po::value< unsigned int >(&(m_fb_dump_pixel_size)),
	 "Pixel size for filter representation in the image dump")
	("fb_img_dump_directory", po::value< std::string >(&(m_fb_img_dump_directory)),
	 "Directory where filter banks will be dumped in image format")
	("fb_txt_dump_directory", po::value< std::string >(&(m_fb_txt_dump_directory)),
	 "Directory where filter banks will be dumped in text format")
	("n_iterations_save", po::value< unsigned int >(&(m_n_iterations_save)),
	 "Number of iterations before dumping filter bank to a file");

  // Parse config file
  std::ifstream ifs(filter_bank_config_filename.c_str());
  if (!ifs.is_open()) {
	throw ConfigParseException("Unable to read the filter bank configuration file");
  }
  po::variables_map cfg_file_vm;
  po::store(po::parse_config_file(ifs,cfg_file_descr),cfg_file_vm);
  po::notify(cfg_file_vm);
  ifs.close();
}

void STD_filter_bank_2D_SS::store_simulation_data(const unsigned int iteration_number,const std::string& fb_txt_filename) const {
  char sim_filename[MAX_FILENAME_SIZE];
  snprintf(sim_filename, MAX_FILENAME_SIZE,"%s/simulation_data.txt",m_results_directory.c_str());
  std::ofstream sim_fd(sim_filename);
  if (!sim_fd.is_open()) {
	throw FileException("Unable to open the simulation data file");
  }
  sim_fd << "filter_bank_path = " << fb_txt_filename << std::endl;
  sim_fd << "iteration_number = " << iteration_number << std::endl;
  sim_fd.close();

  char img_fname[MAX_FILENAME_SIZE];
  char txt_fname[MAX_FILENAME_SIZE];
  snprintf(img_fname,MAX_FILENAME_SIZE,"%s/current_fb.png",m_results_directory.c_str());
  cv::imwrite(img_fname,get_filter_bank_representation());
  snprintf(txt_fname,MAX_FILENAME_SIZE,"%s/current_fb.txt",m_results_directory.c_str());
  dump_fb_to_txt(txt_fname);
}

void STD_filter_bank_2D_SS::store_filter_bank(const unsigned int iteration_number) const {
  if (iteration_number%m_n_iterations_save==0) {
	std::cerr << "  Iteration " << iteration_number << ", filter bank stored to disk" << std::endl;
	std::string fb_txt_filename = save_filter_bank_to_txt(iteration_number/m_n_iterations_save);
	save_filter_bank_to_img(iteration_number/m_n_iterations_save);
	store_simulation_data(iteration_number,fb_txt_filename);
  }
}

void STD_filter_bank_2D_SS::update_filters(const std::vector< cv::Mat >& filters_gradient) {
#pragma omp parallel for schedule(dynamic)
  for (unsigned int i_filter=0;i_filter<m_filters_no;++i_filter) {
	m_filters.at(i_filter) += filters_gradient.at(i_filter);
  }
  normalize();
}

void STD_filter_bank_2D_SS::dump_fb_to_txt(const char* out_filename) const {

  FILE *fp = fopen(out_filename,"wt");
  if (fp==NULL) {
	std::string err_msg("Impossible to open the file "+std::string(out_filename)+" for dumping the filter bank");
	throw(FileException(err_msg.c_str()));
  }
  // Save filter bank type, cardinality, and size first
  fprintf(fp,"STD_filter_bank_2D_SS\n%d\n%d\n",m_filters_no,m_filters_size);
  for (std::vector< cv::Mat >::const_iterator fb_it=m_filters.begin();fb_it!=m_filters.end();++fb_it) {
	for (int r=0;r<fb_it->rows;++r) {
	  const float *Mr = fb_it->ptr< float >(r);
	  for (int c=0;c<fb_it->cols;++c) {
		if (fprintf(fp,"%f ",Mr[c])<0) {
		  std::string err_msg("Unexpected end of file encountered while writing a filter bank to file "+std::string(out_filename));
		  throw(FileException(err_msg.c_str()));
		}
	  }
	  fprintf(fp,"\n");
	}
  }
  fclose(fp);
}

std::string STD_filter_bank_2D_SS::save_filter_bank_to_txt(const unsigned int iteration_number) const {
  char out_filename[MAX_FILENAME_SIZE];
  snprintf(out_filename,MAX_FILENAME_SIZE,"%s/fb_%06d.txt",m_fb_txt_dump_directory.c_str(),iteration_number);
  dump_fb_to_txt(out_filename);
  return(std::string(out_filename));
}

void STD_filter_bank_2D_SS::save_filter_bank_to_img(const unsigned int iteration_number) const {
  char out_filename[MAX_FILENAME_SIZE];
  snprintf(out_filename,MAX_FILENAME_SIZE,"%s/fb_%06d.png",m_fb_img_dump_directory.c_str(),iteration_number);
  cv::imwrite(out_filename,get_filter_bank_representation());
}

cv::Mat STD_filter_bank_2D_SS::get_filter_bank_representation() const {
  const int n_blocks = m_filters_no;
  // Number of rows of blocks
  const int n_block_rows = ceil(sqrt(n_blocks));
  // Number of columns of blocks
  const int n_block_cols = n_block_rows;
  // Number of rows in each block (they are assumed to be equal)
  const int n_rows = m_filters.at(0).rows;
  // Number of columns in each block (they are assumed to be equal)
  const int n_cols = m_filters.at(0).cols;

  cv::Mat image_bank(n_block_rows*n_rows*m_fb_dump_pixel_size+(n_block_rows-1)*m_fb_dump_spacing,
					 n_block_cols*n_cols*m_fb_dump_pixel_size+(n_block_cols-1)*m_fb_dump_spacing,
					 CV_8UC1,cv::Scalar::all(255));
  int rowN = 0;
  int colN = 0;

  for (std::vector< cv::Mat >::const_iterator it=m_filters.begin();it!=m_filters.end();++it) {
	cv::Mat tmp = Utils::convert_img_visualization(Utils::magnify_matrix(*it,m_fb_dump_pixel_size));
	if (colN==n_block_cols) {
	  ++rowN;
	  colN = 0;
	}

	cv::Mat image_bank_roi = image_bank(cv::Range(rowN*n_rows*m_fb_dump_pixel_size+rowN*m_fb_dump_spacing,(rowN+1)*n_rows*m_fb_dump_pixel_size+rowN*m_fb_dump_spacing),
										cv::Range(colN*n_cols*m_fb_dump_pixel_size+colN*m_fb_dump_spacing,(colN+1)*n_cols*m_fb_dump_pixel_size+colN*m_fb_dump_spacing));
	tmp.copyTo(image_bank_roi);
	++colN;
  }

  return(image_bank);
}

void STD_filter_bank_2D_SS::load_filter_bank_from_file(const std::string& fb_filename) {
  m_filters.clear();

  // Use C interface with files (more reliable)
  FILE *fp;
  if ((fp=fopen(fb_filename.c_str(),"rt"))==NULL) {
	std::string err_msg("Unable to load matrix from file -- impossible to open file "+fb_filename);
	throw(PathException(err_msg.c_str()));
  }

  char fb_type[DATASET_TYPE_STRLEN];
  memset(fb_type,'\0',DATASET_TYPE_STRLEN*sizeof(char));
  fscanf(fp,"%s\n",fb_type);
  if (std::string(fb_type)!="STD_filter_bank_2D_SS") {
	std::string err_msg("Wrong filter bank type: "+std::string(fb_type));
	throw FilterBankException(err_msg.c_str());
  }

  unsigned int filters_no=0, filters_size=0;
  fscanf(fp,"%d\n",&filters_no);
  fscanf(fp,"%d\n",&filters_size);
  std::cerr << "    Read " << filters_no << " filters, size " << filters_size << std::endl;
  if (filters_no==0 || filters_size==0) {
	throw FilterBankException("Loaded filter bank has invalid filter bank cardinality or size");
  }

  m_filters_size = filters_size;
  m_filters_no = filters_no;

  for (unsigned int i_filter=0;i_filter<filters_no;++i_filter) {
	cv::Mat tmp_mat(filters_size,filters_size,CV_32FC1);
	for (int r=0;r<tmp_mat.rows;++r) {
	  float *Mr = tmp_mat.ptr< float >(r);
	  for (int c=0;c<tmp_mat.cols;++c) {
		if (fscanf(fp,"%f",&Mr[c])<0) {
		  std::string err_msg("Unexpected end of file encountered while reading a filter bank from file "+fb_filename);
		  throw(ProcessingException(err_msg.c_str()));
		}
	  }
	}
	m_filters.push_back(tmp_mat);
  }

  fclose(fp);
}
