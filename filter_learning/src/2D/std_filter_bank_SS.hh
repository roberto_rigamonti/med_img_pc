/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef STD_FILTER_BANK_2D_SS_HH
#define STD_FILTER_BANK_2D_SS_HH

#include <iostream>
#include <fstream>
#include <iterator>
#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>
#include "opencv2/opencv.hpp"

#include "../cmd_line.hh"
#include "../dataset.hh"
#include "../parameters.hh"
#include "../exceptions.hh"
#include "../filter_bank.hh"
#include "../utils.hh"

namespace fs = boost::filesystem;

class STD_filter_bank_2D_SS : public Filter_bank {
public:
  STD_filter_bank_2D_SS(CmdLine& cmd_line,Parameters& config,cv::RNG& rng);
  virtual ~STD_filter_bank_2D_SS() { };

  const_iterator begin() const { return m_filters.begin(); }
  const_iterator end() const { return m_filters.end(); }
  const_iterator element_at(const unsigned int el_number) const { return m_filters.begin()+el_number; }

  cv::Mat get_filter_bank_representation() const;
  void update_filters(const std::vector< cv::Mat >& filters_gradient);
  void store_filter_bank(const unsigned int iteration_number) const;

  /*
	Pushes in the argument vector the dimensions of the filter bank.
	Encoding format:
	- filter bank cardinality
	- number of rows
	- number of columns
  */
  void get_fb_dimensions(struct fb_dimensions& dims) const {
	dims.filters_no = m_filters_no;
	dims.dims_no = 2;
	dims.dims.clear();
	dims.dims.push_back(m_filters_size);
	dims.dims.push_back(m_filters_size);
  }

private:
  unsigned int m_filters_size;
  unsigned int m_filters_no;

  std::vector< cv::Mat > m_filters;

  unsigned int m_fb_dump_spacing;
  unsigned int m_fb_dump_pixel_size;
  std::string m_fb_img_dump_directory;
  std::string m_fb_txt_dump_directory;
  unsigned int m_n_iterations_save;
  bool m_first_filter_uniform;

  std::string m_results_directory;

  void parse_filter_bank_config(const std::string& filter_bank_config_filename);
  void load_filter_bank_from_file(const std::string& fb_filename);
  void dump_fb_to_txt(const char* out_filename) const;
  std::string save_filter_bank_to_txt(const unsigned int iteration_number) const;
  void save_filter_bank_to_img(const unsigned int iteration_number) const;
  void store_simulation_data(const unsigned int iteration_number,const std::string& fb_txt_filename) const;
  void normalize();
};

#endif // STD_FILTER_BANK_2D_SS_HH
