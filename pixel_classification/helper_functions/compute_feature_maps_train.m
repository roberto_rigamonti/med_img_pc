function [] = compute_feature_maps_train(p,i_img)
%  compute_feature_maps_train  Compute the feature maps for the specified image
%                              from the training set
%
%  Synopsis:
%     compute_feature_maps_train(p,i_img)
%
%  Input:
%     p     = structure containing system's configuration and paths
%     i_img = image number

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

img_filename = p.train_imgs_list{i_img};
mask_filename = p.train_masks_list{i_img};
% Destination feature map's file format
fm_file_format = '%s/feature_map_%03d.nrrd';
[img_path,img_name,img_ext] = fileparts(img_filename);
fm_output_dir = fullfile(p.paths.train_fm_dir,img_name);

% Reuse previously-computed feature maps (if their number matches the expected
% one, so as to avoid resuming incomplete sets)
if (exist(fm_output_dir,'dir'))
    file_list = dir(fm_output_dir);
    expected_file_no = p.filter_bank.no;
    if (p.use_oof)
        expected_file_no = expected_file_no+1;
    end
    if (p.use_ef)
        expected_file_no = expected_file_no+1;
    end

    % +2 to account for . and ..
    if (length(file_list)~=expected_file_no+2)
        warning('fm:resume','The feature maps in %s do not match in number (%d) the expected ones (%d) -- recomputing',fm_output_dir,length(file_list)-2,expected_file_no);
        [status,message,messageid] = rmdir(fm_output_dir,'s'); %#ok<*NASGU,*ASGLU>
    end
end

if (~exist(fm_output_dir,'dir'))
    fprintf('     computing the feature maps for image %s...\n',img_name);
    [status,message,messageid] = mkdir(fm_output_dir); %#ok<*NASGU,*ASGLU>

    img = im2double(imread(img_filename));
    if (size(img,3)>1) 
    	img = rgb2gray(img);
    end
    mask = im2double(imread(mask_filename));
    if (size(mask,3)>1)
	mask = rgb2gray(mask);    
    end
    if (~islogical(mask))
        mask = im2bw(mask);
    end
   
    fm_count = 0;

    % If requested, compute OOF response (it simply accounts to copying the
    % versione already available in the dataset)
    if (p.use_oof)
        oof_response = normalize_in_mask(nrrdLoad(p.train_oof_list{i_img})',mask);
        oof_response(mask==0) = 0;
        nrrdSave(sprintf(fm_file_format,fm_output_dir,fm_count),oof_response');
        fm_count = fm_count+1;
    end

    % If requested, compute EF response (it simply accounts to copying the
    % versione already available in the dataset)
    if (p.use_ef)
        ef_response = normalize_in_mask(nrrdLoad(p.train_ef_list{i_img})',mask);
        ef_response(mask==0) = 0;
        nrrdSave(sprintf(fm_file_format,fm_output_dir,fm_count),ef_response');
        fm_count = fm_count+1;
    end

    % Compute responses from filter banks, keeping whitening into account
    if (p.filter_bank.no>0)
        fm_count = compute_feature_maps(p,img,mask,fm_output_dir,fm_file_format,fm_count);
    end
else
    fprintf('     the feature maps for image %s have already been computed, skipping...\n',img_name);
end

end
