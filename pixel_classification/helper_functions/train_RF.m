function [model] = train_RF(p,train_data_filename)
%  train_RF  Train a Random Forest classifier on the given training data
%
%  Synopsis:
%     [model] = train_RF(p,train_data_filename)
%
%  Input:
%     p                   = structure containing system's configuration and
%                           paths
%     train_data_filename = name of the file containing the training data
%  Output:
%     model = structure containing the trained Random Forest classifier

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

fprintf('  Training Random Forest classifier...\n');

train_data = load(train_data_filename);

% The labels are put as last column of the training data
vals = train_data(:,1:end-1);
labels = train_data(:,end);

opts = [];
opts.class_names = [0 1];  
opts.sample_classes_individually = true;

% Perform training
[model,oob] = vigraLearnRF(vals,labels,p.rf.trees_no,opts); %#ok<NASGU>

end
