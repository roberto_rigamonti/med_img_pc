function [test_data_files,test_labels_files] = get_test_samples(p)
%  get_test_samples  Convert the feature maps in a format suitable for the
%                    classifiers
%
%  Synopsis:
%     [test_data_files,test_labels_files] = get_test_samples(p)
%
%  Input:
%     p = structure containing system's configuration and paths
%  Output:
%     test_data_files   = list of files containing the test data
%     test_labels_files = list of files containing the test labels

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 10 September 2012

% Get the list of directories containing the feature maps of test images
[test_dirs_list,test_dirs_no] = get_list(p.paths.test_fm_list);

test_data_files = cell(test_dirs_no,1);
test_labels_files = cell(test_dirs_no,1);

parfor i_test = 1:test_dirs_no
    % Get feature maps' list
    fm_dir = test_dirs_list{i_test};
    fm_list = dir(fullfile(fm_dir,'*.nrrd'));
    fm_no = length(fm_list);
    % Load the first feature map to get their size and allocate memory for loading them
    fm = nrrdLoad(fullfile(fm_dir,fm_list(1).name))';
    rows = size(fm,1);
    cols = size(fm,2);
    feature_maps = zeros(rows,cols,fm_no);
    % Initialize files for data/labels
    test_data_files{i_test} = sprintf('%s/test_%03d.txt',p.paths.test_data,i_test); %#ok<*PFBNS>
    test_labels_files{i_test} = sprintf('%s/test_%03d.txt',p.paths.test_labels,i_test);

    fd_labels = fopen(test_labels_files{i_test},'wt');

    % Load feature maps
    for i_fm = 1:fm_no
        feature_maps(:,:,i_fm) = double(nrrdLoad(fullfile(fm_dir,fm_list(i_fm).name))');
        
        if (sum(sum(isnan(feature_maps(:,:,i_fm))))>0)
            error('NaN: test dir %d, fm %d\n',i_test,i_fm);
        end 
    end
    % Load ground truth
    gt = double(im2bw(imread(p.test_gt_list{i_test})));
    
    if(strcmp(p.classifier,'l1reg'))
        fprintf(fd_labels,'%%%%MatrixMarket matrix array real general\n%d %d\n',rows*cols,1);
    end
    
    % Dump data in the requested format
    switch p.classifier
        case 'RF'
            for r = 1:rows
                for c = 1:cols
                    fprintf(fd_labels,'%d\n',gt(r,c));
                end
            end
        case 'l1reg'
            % In this case, no labels in the data file
            gt(gt==0) = -1;
            gt = gt';
            fprintf(fd_labels,'%d\n',gt(:));
    end

    write_test_file(feature_maps,gt,test_data_files{i_test},p.classifier);
    fclose(fd_labels);
end

end
