function [p] = get_config(fb_name,use_oof,use_ef,classifier)
%  get_config  Setup framework's configuration and paths
%
%  Synopsis:
%     [p] = get_config(fb_name,use_oof,use_ef,classifier)
%
%  Input:
%     fb_name    = name of the filter bank used for feature extraction
%     use_oof    = flag used to enable the use of OOF in classification
%     use_ef     = flag used to enable the use of EF in classification
%     classifier = name of the classifier requested by the user
%  Output:
%     p = structure containing system's configuration and paths

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 26 June 2012

%% Feature extraction parameters
p.fb_name = fb_name;
p.use_oof = use_oof;
p.use_ef = use_ef;

%% Dataset configuration
p.train_dataset_name = 'DRIVE';
p.test_dataset_name = 'DRIVE';

%% Classifier configuration
% Classifier type (valid values: RF, l1reg)
p.classifier = classifier;
% Number of trees for a Random Forest classifier (600 is more than enough in
% most cases)
p.rf.trees_no = 600;
% Paths for l1-regularized regressor
p.l1_train_path = 'libs/l1reg/l1_logreg_train';
p.l1_test_path = 'libs/l1reg/l1_logreg_classify';
% Regularization parameter for l1-regularized regression
p.l1reg.lambda = 0.01;
% Number of training samples. Normally 10000 training samples should be ok
% (consider that this number will be doubled, as we have both positive and 
% negative samples).
% Please note that, using a huge number of samples, e.g., 200000, a filter bank
% composed by many filters with RF might outperform a smaller one combined with
% OOF and EF on some datasets (e.g., DRIVE), but the training/testing times grow
% considerably.
p.train_samples_no = 10000;
% Number of repetitions that have to be performed on the whole training set
% (useful to randomize the results).
p.test_repetitions_no = 1;

%% Results configuration
% Number of thresholds used in the computations of the statistics
p.results_thresholds_no = 500;

%% Setup the paths and create directories
% Compute experiment's codenames (they are used to build the paths and avoid
% accidentally deleting some previous results)
p.data_codename = ['TR_' p.train_dataset_name '__TS_' p.test_dataset_name];
p.var_codename = ['fb_' p.fb_name '__' sprintf('OOF_%d_EF_%d',p.use_oof,p.use_ef)];
p.res_codename = [p.classifier sprintf('_SN_%d_REP_%d',p.train_samples_no,p.test_repetitions_no) '__' p.var_codename];

% Setup directories used for storing temporary results (var) and final results
% (res)
p.paths.var = fullfile('var',p.data_codename,p.var_codename);
p.paths.res = fullfile('results',p.data_codename,p.res_codename);

[status,message,messageid] = mkdir(p.paths.var);
if (exist(p.paths.res,'dir'))
    user_choice = input('A previous results directory corresponding to the same experiment codename exists, should I go on and delete it? (yes/no) ','s');
    if(~strcmpi(user_choice,'yes'))
        error('Simulation CANCELED by user to preserve results directory');
    end
    
    fprintf('REMOVING previously existing results path\n');
    [status,message,messageid] = rmdir(p.paths.res,'s'); %#ok<*NASGU,*ASGLU>
end
[status,message,messageid] = mkdir(p.paths.res);

% Filter bank's path
p.paths.fb = fullfile('data',p.train_dataset_name,'filter_banks',sprintf('%s.txt',p.fb_name));

% Files containing the list of files contained in the dataset -- please be sure
% to respect this convention!
p.paths.test_imgs_list = fullfile('data',p.test_dataset_name,'test_imgs.txt');
p.paths.train_imgs_list = fullfile('data',p.train_dataset_name,'train_imgs.txt');
p.paths.test_masks_list = fullfile('data',p.test_dataset_name,'test_masks.txt');
p.paths.train_masks_list = fullfile('data',p.train_dataset_name,'train_masks.txt');
p.paths.pos_sampling_list = fullfile('data',p.train_dataset_name,'pos_sampling_masks.txt');
p.paths.neg_sampling_list = fullfile('data',p.train_dataset_name,'neg_sampling_masks.txt');
p.paths.test_gt_list = fullfile('data',p.test_dataset_name,'test_gt.txt');
if (p.use_oof)
    p.paths.train_oof_list = fullfile('data',p.train_dataset_name,'train_oof.txt');
    p.paths.test_oof_list = fullfile('data',p.test_dataset_name,'test_oof.txt');
end
if (p.use_ef)
    p.paths.train_ef_list = fullfile('data',p.train_dataset_name,'train_ef.txt');
    p.paths.test_ef_list = fullfile('data',p.test_dataset_name,'test_ef.txt');
end

% Paths of intermediate files/directories
p.paths.responses = fullfile(p.paths.res,'responses');
p.paths.test_fm_list = fullfile(p.paths.res,'test_list.txt');
p.paths.train_fm_list = fullfile(p.paths.res,'train_list.txt');
p.paths.test_fm_dir = fullfile(p.paths.var,'feature_maps','test');
if (~exist(p.paths.test_fm_dir,'dir'))
    [status,message,messageid] = mkdir(p.paths.test_fm_dir); %#ok<NASGU,ASGLU>
end
p.paths.train_fm_dir = fullfile(p.paths.var,'feature_maps','train');
if (~exist(p.paths.train_fm_dir,'dir'))
    [status,message,messageid] = mkdir(p.paths.train_fm_dir); %#ok<NASGU,ASGLU>
end
p.paths.test_data = fullfile(p.paths.res,'test_data');
p.paths.train_data = fullfile(p.paths.res,'train_data');
p.paths.test_labels = fullfile(p.paths.res,'test_labels');
p.paths.train_labels = fullfile(p.paths.res,'train_labels');

% Create the missing directories
if (~exist(p.paths.responses,'dir'))
    [status,message,messageid] = mkdir(p.paths.responses); %#ok<NASGU,ASGLU>
end
if (~exist(p.paths.test_data,'dir'))
    [status,message,messageid] = mkdir(p.paths.test_data); %#ok<NASGU,ASGLU>
end
if (~exist(p.paths.train_data,'dir'))
    [status,message,messageid] = mkdir(p.paths.train_data); %#ok<NASGU,ASGLU>
end
if (~exist(p.paths.test_labels,'dir'))
    [status,message,messageid] = mkdir(p.paths.test_labels); %#ok<NASGU,ASGLU>
end
if (~exist(p.paths.train_labels,'dir'))
    [status,message,messageid] = mkdir(p.paths.train_labels); %#ok<NASGU,ASGLU>
end

% Get the lists of dataset's files
[p.train_imgs_list,p.train_imgs_no] = get_list(p.paths.train_imgs_list);
[p.train_masks_list,p.train_masks_no] = get_list(p.paths.train_masks_list);
[p.pos_sampling_list,p.pos_sampling_no] = get_list(p.paths.pos_sampling_list);
[p.neg_sampling_list,p.neg_sampling_no] = get_list(p.paths.neg_sampling_list);
[p.test_imgs_list,p.test_imgs_no] = get_list(p.paths.test_imgs_list);
[p.test_masks_list,p.test_masks_no] = get_list(p.paths.test_masks_list);
[p.test_gt_list,p.test_gt_no] = get_list(p.paths.test_gt_list);
if (p.use_oof)
    [p.train_oof_list,p.train_oof_no] = get_list(p.paths.train_oof_list);
    [p.test_oof_list,p.test_oof_no] = get_list(p.paths.test_oof_list);
else
    [p.train_oof_list,p.train_oof_no] = deal({},0);
    [p.test_oof_list,p.test_oof_no] = deal({},0);
end
if (p.use_ef)
    [p.train_ef_list,p.train_ef_no] = get_list(p.paths.train_ef_list);
    [p.test_ef_list,p.test_ef_no] = get_list(p.paths.test_ef_list);
else
    [p.train_ef_list,p.train_ef_no] = deal({},0);
    [p.test_ef_list,p.test_ef_no] = deal({},0);
end

end
