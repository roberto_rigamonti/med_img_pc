function [train_data_filename,train_labels_filename] = get_train_samples(p,i_rep)
%  get_train_samples  Convert the feature maps in a format suitable for the
%                     classifiers
%
%  Synopsis:
%     [train_data_filename,train_labels_filename] = get_train_samples(p)
%
%  Input:
%     p     = structure containing system's configuration and paths
%     i_rep = iteration number (for multiple random tests)
%  Output:
%     train_data_filename   = file containing the training data for the given
%                             round 
%     train_labels_filename = file containing the training labels for the given
%                             round 

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 08 November 2012

[train_dirs_list,train_dirs_no] = get_list(p.paths.train_fm_list);

train_data_filename = fullfile(p.paths.train_data,sprintf('train_round_%d.txt',i_rep));
train_labels_filename = fullfile(p.paths.train_labels,sprintf('train_round_%d.txt',i_rep));
if (~strcmp(p.classifier,'l1reg'))
    fd_data = fopen(train_data_filename,'wt');
    fd_labels = fopen(train_labels_filename,'wt');
else
    SAMPLES = cell(train_dirs_no,1);
    LABELS = cell(train_dirs_no,1);
end

samples_collected_no = 0;

for i_dir = 1:train_dirs_no
    % Number of samples to collect on this image
    if (i_dir<train_dirs_no)
        N = floor(p.train_samples_no/train_dirs_no);
    else
        N = p.train_samples_no-samples_collected_no;
    end
    samples_collected_no = samples_collected_no+N;

    % Load pos/neg sampling areas and the corresponding fms
    pos_mask = im2double(imread(p.pos_sampling_list{i_dir}));
    neg_mask = im2double(imread(p.neg_sampling_list{i_dir}));

    pos_idx = find(pos_mask~=0);
    neg_idx = find(neg_mask~=0);
    
    pos_swapping = randi(length(pos_idx),N,1);
    neg_swapping = randi(length(neg_idx),N,1);
    pos_samples_coord = pos_idx(pos_swapping);
    neg_samples_coord = neg_idx(neg_swapping);
    
    % Load feature maps
    fm_dir = train_dirs_list{i_dir};
    fm_list = dir(fullfile(fm_dir,'*.nrrd'));
    fm_no = length(fm_list);
    pos_samples = zeros(N,fm_no);
    neg_samples = zeros(N,fm_no);

    for i_fm = 1:fm_no
        fm = nrrdLoad(fullfile(fm_dir,fm_list(i_fm).name))';
        
        if (sum(sum(isnan(fm)))>0)
            error('NaN: train dir %d, fm %d\n',i_dir,i_fm);
        end 
        
        pos_samples(:,i_fm) = fm(pos_samples_coord);
        neg_samples(:,i_fm) = fm(neg_samples_coord);        
    end

    switch p.classifier
        case 'RF'
            for i_sample = 1:N
                fprintf(fd_data,'%f, ',pos_samples(i_sample,:));
                fprintf(fd_data,'1\n');
                fprintf(fd_labels,'1\n');
                fprintf(fd_data,'%f, ',neg_samples(i_sample,:));
                fprintf(fd_data,'0\n');
                fprintf(fd_labels,'0\n');
            end
        case 'l1reg'
            SAMPLES{i_dir} = [pos_samples;neg_samples];
            LABELS{i_dir} = [ones(N,1);-ones(N,1)];
    end
end

if (~strcmp(p.classifier,'l1reg'))
    fclose(fd_data);
    fclose(fd_labels);
else
    mmwrite(train_data_filename,cell2mat(SAMPLES));
    mmwrite(train_labels_filename,cell2mat(LABELS));
end

end
