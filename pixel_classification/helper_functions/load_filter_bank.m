function [filter_bank] = load_filter_bank(p)
%  load_filter_bank  Load a filter bank from file
%
%  Synopsis:
%     [filter_bank] = load_filter_bank(p)
%
%  Input:
%     p = structure containing system's configuration and paths
%  Output:
%     filter_bank = structure containing a filter bank along with its size and
%                   the number of filters that compose it

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

if (~isempty(p.fb_name))
    fb_file = load(p.paths.fb);
    filter_size = size(fb_file,2);
    if (rem(size(fb_file,1),filter_size)~=0)
        error('The specified filter bank (%s) has non-square filters',p.paths.fb);
    end
    filter_bank.no = size(fb_file,1)/filter_size;
    filter_bank.size = filter_size;
    
    filter_bank.fb = cell(filter_bank.no,1);
    for i_f = 1:filter_bank.no
        filter_bank.fb{i_f} = fb_file((i_f-1)*filter_bank.size+1:i_f*filter_bank.size,:);
    end
else
    filter_bank.fb = [];
    filter_bank.no = 0;
    filter_bank.size = 0;
end

end
