function [fm_count] = compute_feature_maps(p,img,mask,fm_output_dir,fm_file_format,fm_count)
%  compute_feature_maps  Extracts the feature maps from a given image using the
%                        specified filter bank
%
%  Synopsis:
%     [fm_count] = compute_feature_maps(p,img,mask,fm_output_dir,fm_file_format,fm_count)
%
%  Input:
%     p              = structure containing system's configuration and paths
%     img            = source image for feature maps' computation
%     mask           = mask for the input image
%     fm_output_dir  = directory where the feature maps are stored
%     fm_file_format = file format for the saved feature maps
%     fm_count       = feature maps' count (used to generate the progressive
%                      filename)
%  Output:
%     fm_count = incremental counter for the feature maps

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 21 June 2012

fb = p.filter_bank.fb;
filters_no = p.filter_bank.no;
feature_maps = cell(filters_no,1);

% Normalize the image before filtering
img = normalize_in_mask(img,mask);

for i_fm = 1:filters_no
    % Perform the convolution
    feature_maps{i_fm} = imfilter(img,fb{i_fm},'same','corr');

    % Re-normalize the feature map
    feature_map = normalize_in_mask(feature_maps{i_fm},mask);
        
    % Dump response to file
    feature_map(mask==0) = 0;
    nrrdSave(sprintf(fm_file_format,fm_output_dir,fm_count),feature_map');
    fm_count = fm_count+1;
end

end
